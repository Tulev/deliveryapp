## Instalation

- [Clone the repo.](https://Tulev@bitbucket.org/Tulev/deliveryapp.git).

The project uses the [shipping-docker/vessel project](https://vessel.shippingdocker.com)

To get 'vesssel' localy use:

'docker run --rm -it \
    -v $(pwd):/opt \
    -w /opt shippingdocker/php-composer:latest \
    composer require shipping-docker/vessel'

Then:

bash vessel init

Next: Change your settings in the .env file according to your preferences. Some of my settings are placed in the .env.example file for reference. It is important to set these two before you run './vessel start' command:

APP_PORT=XXXX
MYSQL_PORT=XXXXX

and also make sure no other app is using these ports.

Then './vessel start' and you should be good to go!

All artisan commands must start with './vessel ' in order to run them in the containers provided by Vessel.

Install composer and npm dependencies. Migrate your database. There is 'Initiall Database Seeder' that will create 10 orders along with 10 customers and 10 restaurants that you can use to test through the rest of the app.

## Decisions and Solution

A 'POST' request to '/api/orders' with valid customer_id and restaurant_id will create a new order and start the process of creating notifications and sending messages as described in the task. The 'Job' which is responsible for the process will be thrown to a queue if you had run:

'./vessel php artisan queue:work'

In case the message is sent successfully a notification will be saved to the database. Right after that a Job responsible for the 'delayed/confirmation' message will be thrown to the queue.

In case of failed message delivery a notification will be saved to the database.

These messages are accessible through 'GET' request to '/messages'. Here a Vue.js component will display all the data as required per the task. Quick (and elementary) search and also filter by type are also implemented.

A couple of tests are also applied.
