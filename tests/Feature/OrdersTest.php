<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Jobs\ProcessNewOrder;
use Illuminate\Support\Facades\Bus;
use App\Notifications\OrderConfirmationMessage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrdersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_order_can_be_created()
    {
        $this->withoutExceptionHandling();

        $customer = factory('App\Customer')->create();
        $restaurant = factory('App\Restaurant')->create();

        $attributes = [
            'customer_id' => $customer->id,
            'restaurant_id' => $restaurant->id,
        ];

        Bus::fake();

        $this->post('api/orders', $attributes);

        Bus::assertDispatched(ProcessNewOrder::class);

        $this->assertDatabaseHas('orders', $attributes);
    }

    /** @test */
    public function an_order_requires_a_valid_customer()
    {
        $this->withoutExceptionHandling();

        $restaurant = factory('App\Restaurant')->create();

        $response = $this->post('api/orders', [
                'customer_id' => 1,
                'restaurant_id' => $restaurant->id
            ]);

        $response->assertJson(["errors" => ["customer_id" => ["The selected customer id is invalid."]]], 422);
    }

    /** @test */
    public function an_order_requires_a_valid_restaurant()
    {
        $this->withoutExceptionHandling();

        $customer = factory('App\Customer')->create();

        $response = $this->post('api/orders', [
                'customer_id' => $customer->id,
                'restaurant_id' => 1,
            ]);

        $response->assertJson(["errors" => ["restaurant_id" => ["The selected restaurant id is invalid."]]], 422);
    }

    /** @test */
    public function the_customer_is_notified_when_an_order_is_created()
    {
        $this->withoutExceptionHandling();

        $customer = factory('App\Customer')->create(['phone_number' => '00359878517003']);
        $restaurant = factory('App\Restaurant')->create();

        $attributes = [
            'customer_id' => $customer->id,
            'restaurant_id' => $restaurant->id,
        ];

        Notification::fake();

        $this->post('api/orders', $attributes);

        Notification::assertSentTo($customer, OrderConfirmationMessage::class);
    }
}
