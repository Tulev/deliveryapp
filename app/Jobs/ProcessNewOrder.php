<?php

namespace App\Jobs;

use App\Order;
use App\Customer;
use Illuminate\Bus\Queueable;
use Nexmo\Laravel\Facade\Nexmo;
use App\Jobs\AfterMealDeliveryMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\OrderConfirmationMessage;

class ProcessNewOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    protected $customer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order, Customer $customer)
    {
        $this->order = $order;
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->getData();

        Nexmo::message()->send([
            'to' => $this->customer->phone_number,
            'from' => env('APP_NAME'),
            'text' => $data['message_content'],
        ]);

        $this->customer->notify(new OrderConfirmationMessage($data));

        // Delay time is replaced by 2 min in order to easily test sending messages.
        // $delay = $this->restaurant->delivery_time
        AfterMealDeliveryMessage::dispatch($this->customer)->delay(now()->addMinutes(2));;

        return;
    }

    public function failed(\Exception $e = null)
    {
        return $this->customer->notify(
            new OrderConfirmationMessage($this->getData(
                $message_delivery_status = 'failed'),
                $message_delivered = false)
        );
    }

    private function getData($message_delivery_status = 'sent') {

        return $data = [
            'order_id' => $this->order->id,
            'customer_id' => $this->customer->id,
            'customer_phone_number' => $this->customer->phone_number,
            'message_content' => $this->buildMessageContent(),
            'restaurant' => $this->order->restaurant->name,
            'estimated_delivery_time' => $this->order->restaurant->delivery_time,
            'message_delivery_status' => $message_delivery_status,
        ];
    }

    private function buildMessageContent() {
        return 'Your order from '
                . $this->order->restaurant->name
                . ' has been accepted! Time to deliver: '
                .  $this->order->delivery_time . '. ';
    }
}
