<?php

namespace App\Http\Controllers\API;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->typeFilter === 'failed') {
            $records = Message::recentlyFailed()
                                ->get()
                                ->map(function ($message) {
                                    $result = json_decode($message->data, true);
                                    $result['type'] = 'failed';

                                    return $result;
                                });
        } else {
            $records = Message::latest()->take(50)
                                ->get()
                                ->map(function ($message) {
                                    $result = json_decode($message->data, true);
                                    $result['type'] = 'recent';

                                    return $result;
                                });
        }

        return response()->json([
            'data' => [
                'displayable' => $this->getDisplayableColumns(),
                'custom_columns' => $this->getCustomColumnNames(),
                'records' => $records,
            ],
        ]);
    }

    public function getDisplayableColumns() {
        return [
            "order_id",
            "customer_id",
            "customer_phone_number",
            "message_content",
            "restaurant",
            "estimated_delivery_time",
            "message_delivery_status",
            "type"
        ];
    }

    public function getCustomColumnNames() {
        return [
            "order_id" => "Order",
            "customer_id" => "Customer",
            "customer_phone_number" => "Phone",
            "message_content" => "Message",
            "restaurant" => "Restaurant",
            "estimated_delivery_time" => "Delivery Time",
            "message_delivery_status" => "Status",
            "type" => "Type"
        ];
    }
}
