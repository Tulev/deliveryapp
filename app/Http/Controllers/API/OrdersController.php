<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\Customer;
use Illuminate\Http\Request;
use App\Jobs\ProcessNewOrder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:customers,id',
            'restaurant_id' => 'required|exists:restaurants,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $order = Order::create([
            'customer_id' => $request->get('customer_id'),
            'restaurant_id' => $request->get('restaurant_id'),
        ]);

        $customer = Customer::find($request->get('customer_id'));

        ProcessNewOrder::dispatch($order, $customer);

        return response()->json(['message'=>'success'], 200);
    }
}
