<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    public function restaurant() {
        return $this->belongsTo('App\Restaurant');
    }

    public function getDeliveryTimeAttribute() {
        return \Carbon\Carbon::parse($this->created_at)->addSeconds($this->restaurant->delivery_time)->diffForHumans();
    }
}
