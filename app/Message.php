<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'notifications';

    /**
     * Scope a query to only include recently failed messages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRecentlyFailed($query)
    {
        return $query->where('created_at', '>=', now()->subHours(24))
                    ->whereJsonContains('data->message_delivery_status', 'failed');
    }
}
