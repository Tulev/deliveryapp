<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Order's Delivery</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    </head>
    <body>
      <div id="app">
          <div class="row px-4 py-4">
            <div class="col-md-12">
              @yield('content')
            </div>
          </div>
      </div>
      <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
