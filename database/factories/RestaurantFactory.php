<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Restaurant;
use Faker\Generator as Faker;

$factory->define(Restaurant::class, function (Faker $faker) {

    $name = $faker->name;

    return [
        'name'          => $name,
        'slug'          => Str::slug($name, '-'),
        'address'       => $faker->address,
        'delivery_time' => $faker->numberBetween(1200, 4800),
    ];
});
