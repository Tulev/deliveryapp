<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'restaurant_id' => function () {
            return factory(App\Restaurant::class)->create()->id;
        },
        'customer_id' => function () {
            return factory(App\Customer::class)->create(['phone_number' => '00359878517003'])->id;
        },
        'status' => 'pending',
    ];
});
