<?php

Route::post('orders', 'API\OrdersController@store');
Route::get('/messages', 'API\MessagesController@index')->name('messages.index');
